//console.log("Hello World");

/*function printInfo() {
	let nickName = prompt("Enter your nickname:");
	console.log("Hi, " + nickName);
};
printInfo(); //invoke*/

console.log("PARAMETERS AND ARGUMENTS")
function printName(firstName){ //name is the parameter
	console.log("My name is "+ firstName);
};

printName("Juana");//Juana - the argument
printName("Maria");

//Now we have a reusable function/tasl but could have different output based on what value to process with the help of Parameters and Arguments

/*PARAMETER
	"firstName" is called a parameter
	-A parameter acts as named variable/container that exists only inside a function
	-It is used to store information that is provided to a function when it is called/invoke

ARGUMENT
	"Juan", "Maria" are the information/data provided directly into the function, called argument.
	-Values passed when invoking a function are called arguments.
	-These arguments are then stored as the parameter within the function. */

let sampleVariable = "Inday";
printName(sampleVariable);
//variables can also be passed as an argument

console.log("---------------------------------");
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let isDivisibilityBy8 = remainder === 0; // will store a value true/false
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibilityBy8);
};
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*[SECTION] FUNCTION AS ARGUMENT
	-Function parameters can also accept functions as arguments.
	-Some complex functions uses other functions to perform more complicated results. */

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed");
	}

	function invokeFunction(argumentFunction) {
		argumentFunction();
	}

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

console.log("---------------------------------");
/* [SECTION] USING MULTIPLE PARAMETERS */

	function createFullName(firstName, middleName, lastName){
		console.log("My full name is " + firstName + " " + middleName + " " + lastName);
	}

	createFullName("Alissa Mae", "Secretaria", "Benigay");

	function getDifferenceOf8Minus4(numA, numB){
		console.log("Difference: "+ (numA - numB));
	}

	// USING VARIABLE AS AN ARGUMENT
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	getDifferenceOf8Minus4(8,4);
	//getDifferenceOf8Minus4(4,8); // This will result to logical error

console.log("---------------------------------");
/* [SECTION] RETURN STATEMENT
	The return statement allows us to output a value from a function to be passed to the line/block of code that invoked / called */

	function returnFullName(firstName, middleName, lastName){
		//return firstName + " " + middleName + " " + lastName;
		console.log("Printed inside a function");//This line will not be printed

		let fullName = firstName + " " + middleName + " " + lastName;
		return fullName;
	}

	let completeName = returnFullName("Paul", "Smith", "Jordan");
	console.log(completeName);

	console.log("I am " + completeName);

	function printPlayerInfo(userName, level, job){
		console.log("Username: " + userName);
		console.log("Level: " + level);
		console.log("Job: "+ job);
	}

	let user1 = printPlayerInfo("boxzMapagmahal", "Senior", "Programmer");
	console.log(user1);